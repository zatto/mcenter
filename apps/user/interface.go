package user

import (
	"context"

	"gitee.com/zatto/mcenter/apps/domain"
	"gitee.com/zatto/mcenter/common/validator"
	"github.com/infraboard/mcube/http/request"
)

const (
	AppName = "users"
)

type Service interface {
	CreateUser(context.Context, *CreateUserRequest) (*User, error)
	UpdateUser(context.Context, *UpdateUserRequest) (*User, error)
	DeleteUser(context.Context, *DeleteUserRequest) (*User, error)
	RPCServer
}

func NewCreateUserRequest() *CreateUserRequest {
	return &CreateUserRequest{}
}

func (req *CreateUserRequest) Validate() error {
	if req.Domain == "" {
		req.Domain = domain.DEFAULT_DOAMIN
	}
	return validator.V().Struct(req)
}

func NewQueryUserRequest() *QueryUserRequest {
	return &QueryUserRequest{
		Page: request.NewDefaultPageRequest(),
	}
}

func NewDEscribeUserByUsername(val string) *DescribeUserRequest {
	return &DescribeUserRequest{
		DescribeBy:    DESCRIBE_BY_USERNAME,
		DescribeValue: val,
	}
}
