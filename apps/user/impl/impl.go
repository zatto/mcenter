package impl

import (
	"gitee.com/zatto/mcenter/apps/user"
	"gitee.com/zatto/mcenter/conf"
	"github.com/infraboard/mcube/app"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

type impl struct {
	col *mongo.Collection
	user.UnimplementedRPCServer
}

// 实例类初始化
func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	i.col = db.Collection("users")
	return nil
}

func (i *impl) Name() string {
	return user.AppName
}

// 注册grpc
func (i *impl) Registry(server *grpc.Server) {
	user.RegisterRPCServer(server, i)
}

func init() {
	svc := &impl{}
	app.RegistryGrpcApp(svc)
	app.RegistryInternalApp(svc)
}
