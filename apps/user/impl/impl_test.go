package impl_test

import (
	"context"
	"testing"

	"gitee.com/zatto/mcenter/apps/user"
	"gitee.com/zatto/mcenter/test/tools"
	"github.com/infraboard/mcube/app"
)

var (
	svc user.Service
	ctx = context.Background()
)

func TestCreateUser(t *testing.T) {
	req := user.NewCreateUserRequest()
	// req.Username = "admins"
	req.Password = "123456"
	ins, err := svc.CreateUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(ins))
}

func TestUpdateUser(t *testing.T) {
	req := &user.UpdateUserRequest{}
	ins, err := svc.UpdateUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestDeleteUser(t *testing.T) {
	req := &user.DeleteUserRequest{}
	ins, err := svc.DeleteUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestQueryUser(t *testing.T) {
	req := user.NewQueryUserRequest()
	req.Keywords = "adm"
	set, err := svc.QueryUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(set))
}

func TestDescribeUser(t *testing.T) {
	req := &user.DescribeUserRequest{
		DescribeBy:    user.DESCRIBE_BY_USERNAME,
		DescribeValue: "admin",
	}
	ins, err := svc.DescribeUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(ins))
}

func init() {
	tools.DevelopmentSetup()
	svc = app.GetInternalApp(user.AppName).(user.Service)
}
