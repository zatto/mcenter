package user

import (
	"encoding/json"

	"gitee.com/zatto/mcenter/common/meta"
	"golang.org/x/crypto/bcrypt"
)

func NewUserSet() *UserSet {
	return &UserSet{
		Items: []*User{},
	}
}

func (s *UserSet) Add(item *User) {
	s.Items = append(s.Items, item)
}

func NewDefaultUser() *User {
	return &User{
		Meta: meta.NewMeta(),
		Spec: NewCreateUserRequest(),
	}
}

func New(req *CreateUserRequest) (*User, error) {
	// 校验请求参数
	if err := req.Validate(); err != nil {
		return nil, err
	}

	// 密码hash
	if err := req.HashPassword(); err != nil {
		return nil, err
	}

	return &User{
		Meta: meta.NewMeta(),
		Spec: req,
	}, nil
}

func (req *CreateUserRequest) HashPassword() error {
	// 使用bcrypt库加密密码
	hp, err := bcrypt.GenerateFromPassword([]byte(req.Password), 10)
	if err != nil {
		return err
	}
	req.Password = string(hp)
	return nil
}

// 自定义序列化方法
func (u *User) MarshalJSON() ([]byte, error) {
	return json.Marshal(
		struct {
			*meta.Meta
			*CreateUserRequest
		}{u.Meta, u.Spec})
}

func (u *User) Desense() {
	u.Spec.Password = "***"
}

func (u *User) CheckPassword(pass string) error {
	return bcrypt.CompareHashAndPassword([]byte(u.Spec.Password), []byte(pass))
}
