package impl_test

import (
	"context"
	"testing"

	"gitee.com/zatto/mcenter/apps/endpoint"
	"gitee.com/zatto/mcenter/test/tools"
	"github.com/infraboard/mcube/app"
)

var (
	svc endpoint.Service
	ctx = context.Background()
)

func TestRegistryEndpoint(t *testing.T) {
	req := endpoint.NewRegistryRequest()
	req.Items = append(req.Items, &endpoint.CreateEndpointRequest{
		ServiceId: "xxx",
		Method:    "POST",
		Path:      "xxx",
		Operation: "xxx",
	})
	set, err := svc.RegistryEndpoint(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

func init() {
	tools.DevelopmentSetup()
	svc = app.GetInternalApp(endpoint.AppName).(endpoint.Service)
}
