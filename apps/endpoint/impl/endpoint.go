package impl

import (
	"context"

	"gitee.com/zatto/mcenter/apps/endpoint"
)

func (i *impl) RegistryEndpoint(ctx context.Context, in *endpoint.RegistryRequest) (*endpoint.EndpointSet, error) {
	set := endpoint.NewEndpintSet()

	// 遍历功能列表
	for m := range in.Items {
		r := in.Items[m]
		ep, err := endpoint.New(r)
		if err != nil {
			return nil, err
		}

		_, err = i.col.InsertOne(ctx, ep)
		if err != nil {
			return nil, err
		}

		set.Items = append(set.Items, ep)
	}
	return set, nil
}
