package impl

import (
	"gitee.com/zatto/mcenter/apps/endpoint"
	"gitee.com/zatto/mcenter/conf"
	"github.com/infraboard/mcube/app"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

type impl struct {
	col *mongo.Collection
	endpoint.UnimplementedRPCServer
}

func (i *impl) Name() string {
	return endpoint.AppName
}

func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	i.col = db.Collection(endpoint.AppName)

	return nil
}

func (i *impl) Registry(server *grpc.Server) {
	endpoint.RegisterRPCServer(server, i)
}

func init() {
	svc := &impl{}
	app.RegistryGrpcApp(svc)
	app.RegistryInternalApp(svc)
}
