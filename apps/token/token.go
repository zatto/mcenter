package token

import (
	"fmt"
	"time"
)

func NewToken() *Token {
	return &Token{
		IssueAt:          time.Now().Unix(),
		AccessExpiredAt:  600,
		RefreshExpiredAt: 600 * 4,
		Status:           &Status{},
		Meta:             map[string]string{},
	}
}

func (t *Token) CheckAliable() error {
	// 检查令牌状态
	if t.Status.IsBlock {
		return fmt.Errorf(t.Status.BlockReason)
	}

	// 检查令牌是否过期
	dura := time.Since(t.ExpiredTime())
	if dura > 0 {
		return fmt.Errorf("令牌已过期")
	}

	return nil
}

func (t *Token) ExpiredTime() time.Time {
	dura := time.Duration(t.AccessExpiredAt * int64(time.Second))
	return time.Unix(t.IssueAt, 0).Add(dura)
}
