package token

const (
	// 标准认证头
	TOKEN_HEADER_KRY = "Authorization"

	// 上下文中Token key的名称
	ATTRIBUTE_TOKEN_KEY = "token.mcenter"
)
