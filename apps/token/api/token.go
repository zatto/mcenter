package api

import (
	"gitee.com/zatto/mcenter/apps/token"
	"gitee.com/zatto/mcenter/common/logger"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/exception"
	"github.com/infraboard/mcube/http/restful/response"
)

func (h *handler) IssueToken(r *restful.Request, w *restful.Response) {
	// 获取body参数
	in := token.NewIssueTokenRequest()
	err := r.ReadEntity(in)
	if err != nil {
		// response.Failed(w, err)
		logger.L().Warn().Msgf("请求参数异常: %s", err)
		response.Failed(w, exception.NewBadRequest("请求参数异常"))
		return
	}

	// 颁发token/
	tk, err := h.service.IssueToken(r.Request.Context(), in)
	if err != nil {
		// response.Failed(w, err)
		logger.L().Warn().Msgf("认证失败: %s", err)
		response.Failed(w, exception.NewPermissionDeny("用户名或密码错误"))
		return
	}

	w.WriteEntity(tk)
}
