package api

import (
	"gitee.com/zatto/mcenter/apps/token"
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/app"
)

var (
	h = &handler{}
)

type handler struct {
	service token.Service
}

func (h *handler) Config() error {
	h.service = app.GetInternalApp(token.AppName).(token.Service)
	return nil
}

func (h *handler) Name() string {
	return token.AppName
}

func (h *handler) Version() string {
	return "v1"
}

func (h *handler) Registry(ws *restful.WebService) {
	tags := []string{"登陆"}
	ws.Route(ws.POST("/").To(h.IssueToken).
		Doc("令牌颁发").
		Reads(token.IssueTokenRequest{}).
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Metadata("auth", false).
		Metadata("resource", "令牌").
		Writes(token.Token{}).
		Returns(200, "OK", token.Token{}))
}

func init() {
	app.RegistryRESTfulApp(h)
}
