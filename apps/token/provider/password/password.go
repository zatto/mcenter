package password

import (
	"context"
	"fmt"

	"gitee.com/zatto/mcenter/apps/token"
	"gitee.com/zatto/mcenter/apps/token/provider"
	"gitee.com/zatto/mcenter/apps/user"
	"github.com/infraboard/mcube/app"
	"github.com/rs/xid"
)

type issuer struct {
	user user.Service
}

func (i *issuer) Config() error {
	i.user = app.GetInternalApp(user.AppName).(user.Service)
	return nil
}

func (i *issuer) IssueToken(ctx context.Context, in *token.IssueTokenRequest) (*token.Token, error) {
	// 查询用户
	req := user.NewDEscribeUserByUsername(in.Username)
	u, err := i.user.DescribeUser(ctx, req)
	if err != nil {
		// logger.L().Debug().Msg(err.Error())
		return nil, fmt.Errorf("用户名不存在")
	}

	// 检查密码
	if err := u.CheckPassword(in.Password); err != nil {
		// logger.L().Debug().Msg(err.Error())
		return nil, fmt.Errorf("密码错误")
	}

	// 令牌颁发
	tk := token.NewToken()
	tk.AccessToken = xid.New().String()
	tk.RefreshToken = xid.New().String()
	tk.Username = u.Spec.Username
	tk.UserId = u.Meta.Id

	return tk, nil
}

func init() {
	provider.Registry(token.GRANT_TYPE_PASSWORD, &issuer{})
}
