package provider

import (
	"fmt"

	"gitee.com/zatto/mcenter/apps/token"
)

var (
	store = map[token.GRANT_TYPE]Issuer{}
)

func Registry(key token.GRANT_TYPE, value Issuer) {
	store[key] = value
}

func Get(key token.GRANT_TYPE) (TokenIssuer, error) {
	issuer, exists := store[key]
	if !exists {
		return nil, fmt.Errorf("Issuer not registry: %s", key)
	}
	return issuer, nil
}

func Init() error {
	for _, v := range store {
		if err := v.Config(); err != nil {
			return err
		}
	}
	return nil
}
