package provider

import (
	"context"

	"gitee.com/zatto/mcenter/apps/token"
)

type Issuer interface {
	Config() error
	TokenIssuer
}

type TokenIssuer interface {
	IssueToken(context.Context, *token.IssueTokenRequest) (*token.Token, error)
}
