package impl

import (
	"context"

	"gitee.com/zatto/mcenter/apps/token"
	"gitee.com/zatto/mcenter/apps/token/provider"
	"github.com/infraboard/mcube/exception"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func (i *impl) IssueToken(ctx context.Context, in *token.IssueTokenRequest) (*token.Token, error) {
	// 请求参数校验
	if err := in.Validate(); err != nil {
		return nil, err
	}

	// 颁发令牌
	issuer, err := provider.Get(in.GrantType)
	if err != nil {
		return nil, err
	}
	tk, err := issuer.IssueToken(ctx, in)
	if err != nil {
		return nil, err
	}

	// 存储token
	_, err = i.col.InsertOne(ctx, tk)
	if err != nil {
		return nil, err
	}

	return tk, err
}

func (i *impl) ValidateToken(ctx context.Context, in *token.ValidateTokenRequest) (*token.Token, error) {
	// 查找token
	tk := token.NewToken()
	err := i.col.FindOne(ctx, bson.M{"_id": in.AccessToken}).Decode(tk)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, exception.NewNotFound("Token not found: %s", in.AccessToken)
		}
		return nil, err
	}

	// 检察token有效性
	err = tk.CheckAliable()
	if err != nil {
		return nil, err
	}

	return tk, nil
}
