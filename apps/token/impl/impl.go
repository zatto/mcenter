package impl

import (
	"gitee.com/zatto/mcenter/apps/token"
	"gitee.com/zatto/mcenter/apps/token/provider"
	"gitee.com/zatto/mcenter/conf"
	"github.com/infraboard/mcube/app"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"

	// 初始化provider
	_ "gitee.com/zatto/mcenter/apps/token/provider/all"
)

type impl struct {
	col *mongo.Collection

	token.UnimplementedRPCServer
}

func (i *impl) Name() string {
	return token.AppName
}

func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	i.col = db.Collection("tokens")
	return provider.Init()
}

func (i *impl) Registry(server *grpc.Server) {
	token.RegisterRPCServer(server, i)
}

func init() {
	svc := &impl{}
	app.RegistryGrpcApp(svc)
	app.RegistryInternalApp(svc)
}
