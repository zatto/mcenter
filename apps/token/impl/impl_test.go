package impl_test

import (
	"context"
	"testing"

	"gitee.com/zatto/mcenter/apps/token"
	"gitee.com/zatto/mcenter/test/tools"
	"github.com/infraboard/mcube/app"
)

var (
	svc token.Service
	ctx = context.Background()
)

func TestIssueToken(t *testing.T) {
	req := &token.IssueTokenRequest{}
	req.Username = "admin"
	req.Password = "123456"
	tk, err := svc.IssueToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func TestValidateToken(t *testing.T) {
	req := &token.ValidateTokenRequest{}
	req.AccessToken = "cg4r24gh135gq74h5350"
	tk, err := svc.ValidateToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func init() {
	tools.DevelopmentSetup()
	svc = app.GetInternalApp(token.AppName).(token.Service)
}
