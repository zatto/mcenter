package token

import (
	"context"
	"fmt"
)

const (
	AppName = "tokens"
)

type Service interface {
	// token颁发
	IssueToken(context.Context, *IssueTokenRequest) (*Token, error)
	RPCServer
}

func (req *IssueTokenRequest) Validate() error {
	switch req.GrantType {
	case GRANT_TYPE_PASSWORD, GRANT_TYPE_LDAP:
		if req.Username == "" || req.Password == "" {
			return fmt.Errorf("用户名或密码缺失")
		}
	}
	return nil
}

func NewIssueTokenRequest() *IssueTokenRequest {
	return &IssueTokenRequest{}
}

func NewValidateTokenRequest(accessToken string) *ValidateTokenRequest {
	return &ValidateTokenRequest{
		AccessToken: accessToken,
	}
}
