package impl_test

import (
	"context"
	"testing"

	"gitee.com/zatto/mcenter/apps/service"
	"gitee.com/zatto/mcenter/test/tools"
	"github.com/infraboard/mcube/app"
)

var (
	svc service.ServiceManager
	ctx = context.Background()
)

func init() {
	tools.DevelopmentSetup()
	svc = app.GetInternalApp(service.AppName).(service.ServiceManager)
}

func TestCreateService(t *testing.T) {
	req := &service.CreateServiceRequest{
		Domain:    "default",
		Namespace: "default",
		Name:      "mpass",
	}
	ins, err := svc.CreateService(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestDescribeService(t *testing.T) {
	req := &service.DescribeServiceRequest{
		DescribeBy:    service.DESCRIBE_BY_SERVICE_CREDENTAIL_ID,
		DescribeValue: "cg4psqgh135l9c4als80",
	}
	ins, err := svc.DescribeService(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}
