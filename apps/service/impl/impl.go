package impl

import (
	"gitee.com/zatto/mcenter/apps/service"
	"gitee.com/zatto/mcenter/conf"
	"github.com/infraboard/mcube/app"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

type impl struct {
	service.UnimplementedRPCServer
	col *mongo.Collection
}

func (i *impl) Name() string {
	return service.AppName
}

func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}

	i.col = db.Collection(service.AppName)
	return nil
}

func (i *impl) Registry(server *grpc.Server) {
	service.RegisterRPCServer(server, i)
}

func init() {
	svc := &impl{}
	app.RegistryGrpcApp(svc)
	app.RegistryInternalApp(svc)
}
