package service

import (
	"time"

	"gitee.com/zatto/mcenter/common/meta"
	"github.com/rs/xid"
)

func New(in *CreateServiceRequest) *Service {
	return &Service{
		Meta: meta.NewMeta(),
		Spec: in,
		Credentail: &Credentail{
			ClientId:     xid.New().String(),
			ClientSecret: xid.New().String(),
			CreateAt:     time.Now().Unix(),
		},
	}
}

func NewDefualtService() *Service {
	return New(&CreateServiceRequest{})
}
