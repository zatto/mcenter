# 用户中心
+ 认证
    + 登陆: 获取身份令牌(Token)
      + 基于密码认证
      + 基于ldap认证
      + 基于第三方认证(飞书/钉钉/企业微信)
+ 鉴权

## 技术
+ RESTful接口: 用于Web
  + Go Restful: 专门用于开发Restful API, K8s的API Server 就是用这个框架开发
+ 内部接口: GRPC, 用户认证中间件
  + GRPC 相关开发
+ 数据存储:
  + MongoDB(自己更换成MySQL)