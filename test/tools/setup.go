package tools

import (
	_ "gitee.com/zatto/mcenter/apps"
	"gitee.com/zatto/mcenter/conf"
	"github.com/infraboard/mcube/app"
)

func DevelopmentSetup() {
	// 从unit_test.env加载环境变量
	if err := conf.LoadConfigFromEnv(); err != nil {
		panic(err)
	}

	// 初始化所有app
	if err := app.InitAllApp(); err != nil {
		panic(err)
	}
}
