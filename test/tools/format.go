package tools

import (
	"gitee.com/zatto/mcenter/common/format"
	"gopkg.in/yaml.v3"
)

func MustToYaml(v any) string {
	b, err := yaml.Marshal(v)
	if err != nil {
		panic(err)
	}
	return string(b)
}

func MustToJson(v any) string {
	return format.Prettify(v)
}
