package format

import "encoding/json"

func Prettify(i any) string {
	resp, _ := json.MarshalIndent(i, "", "   ")
	return string(resp)
}
