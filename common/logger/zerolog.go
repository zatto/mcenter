package logger

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/rs/zerolog"
)

func L() *zerolog.Logger {
	return logger
}

var (
	// 保护全局Logger对象
	logger *zerolog.Logger
)

func init() {
	output := zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339}
	output.FormatLevel = func(i interface{}) string {
		return strings.ToUpper(fmt.Sprintf("| %-6s|", i))
	}
	output.FormatMessage = func(i interface{}) string {
		return fmt.Sprintf("%s", i)
	}
	output.FormatFieldName = func(i interface{}) string {
		return fmt.Sprintf("%s:", i)
	}
	output.FormatFieldValue = func(i interface{}) string {
		return strings.ToUpper(fmt.Sprintf("%s", i))
	}

	// zerolog.CallerMarshalFunc = func(pc uintptr, file string, line int) string {
	// 	short := file
	// 	for i := len(file) - 1; i > 0; i-- {
	// 		if file[i] == '/' {
	// 			short = file[i+1:]
	// 			break
	// 		}
	// 	}
	// 	file = short
	// 	return file + ":" + strconv.Itoa(line)
	// }

	l := zerolog.New(output).With().Timestamp().Caller().Logger()
	logger = &l
}
