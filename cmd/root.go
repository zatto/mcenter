package cmd

import (
	"errors"
	"fmt"

	"gitee.com/zatto/mcenter/cmd/start"
	"gitee.com/zatto/mcenter/conf"
	"gitee.com/zatto/mcenter/version"
	"github.com/infraboard/mcube/app"
	"github.com/spf13/cobra"
)

var (
	// pusher service config option
	confType string
	confFile string
	confETCD string
)

var vers bool

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "mcenter",
	Short: "用户中心",
	Long:  "用户中心",
	RunE: func(cmd *cobra.Command, args []string) error {
		if vers {
			fmt.Println(version.FullVersion())
			return nil
		}
		return cmd.Help()
	},
}

func initail() {
	// 加载全局配置
	err := loadGlobalConfig(confType)
	cobra.CheckErr(err)

	// 初始化全局app
	err = app.InitAllApp()
	cobra.CheckErr(err)

}

func loadGlobalConfig(configType string) error {
	switch configType {
	case "file":
		if err := conf.LoadConfigFromToml(confFile); err != nil {
			return err
		}
	case "env":
		if err := conf.LoadConfigFromEnv(); err != nil {
			return err
		}
	default:
		return errors.New("unknow config type")
	}
	return nil
}

func Execute() {
	cobra.OnInitialize(initail)
	RootCmd.AddCommand(start.Cmd)
	err := RootCmd.Execute()
	cobra.CheckErr(err)
}

func init() {
	RootCmd.PersistentFlags().StringVarP(&confType, "config-type", "t", "file", "the service config type [file/env/etcd]")
	RootCmd.PersistentFlags().StringVarP(&confFile, "config-file", "f", "etc/config.toml", "the service config from file")
	RootCmd.PersistentFlags().StringVarP(&confETCD, "config-etcd", "e", "127.0.0.1:2379", "the service config from etcd")
	RootCmd.PersistentFlags().BoolVarP(&vers, "version", "v", false, "the mcenter version")
}
