package rpc

import (
	"context"

	"gitee.com/zatto/mcenter/apps/token"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ClientSet struct {
	conf *Config
	conn *grpc.ClientConn
}

func NewClientSet(conf *Config) (*ClientSet, error) {
	ctx, cancel := context.WithTimeout(context.Background(), conf.Timeout())
	defer cancel()

	// 初始化grpc客户端
	conn, err := grpc.DialContext(
		ctx,
		conf.Address,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithPerRPCCredentials(conf),
	)
	if err != nil {
		return nil, err
	}

	return &ClientSet{
		conf: conf,
		conn: conn,
	}, nil
}

func (c *ClientSet) Token() token.RPCClient {
	return token.NewRPCClient(c.conn)
}
