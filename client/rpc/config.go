package rpc

import (
	"context"
	"time"

	"gitee.com/zatto/mcenter/apps/service"
)

func NewDefaultConfig() *Config {
	return &Config{
		Address:       "localhost:18010",
		TimeoutSecond: 10,
	}
}

type Config struct {
	Address       string `json:"address" toml:"address" yaml:"address" env:"MCENTER_GRPC_ADDRESS"`
	ClientId      string `json:"client_id" toml:"client_id" yaml:"client_id" env:"MCENTER_CLINET_ID"`
	ClientSecret  string `json:"client_secret" toml:"client_secret" yaml:"client_secret" env:"MCENTER_CLIENT_SECRET"`
	TimeoutSecond uint   `json:"timeout_second" toml:"timeout_second" yaml:"timeout_second" env:"MCENTER_GRPC_TIMEOUT_SECOND"`
}

func (c *Config) Timeout() time.Duration {
	return time.Second * time.Duration(c.TimeoutSecond)
}

func (c *Config) GetRequestMetadata(ctx context.Context, uri ...string) (
	map[string]string, error) {
	return map[string]string{
		service.ClientHeaderKey: c.ClientId,
		service.ClientSecretKey: c.ClientSecret,
	}, nil
}

func (c *Config) RequireTransportSecurity() bool {
	return false
}
