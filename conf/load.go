package conf

import (
	"github.com/BurntSushi/toml"
	"github.com/caarlos0/env/v6"
)

var (
	golbal *Config
)

func C() *Config {
	if golbal == nil {
		panic("Load config first")
	}
	return golbal
}

// 从toml文件加载配置
func LoadConfigFromToml(filePath string) error {
	cfg := newDefaultConfig()
	if _, err := toml.DecodeFile(filePath, cfg); err != nil {
		return err
	}
	golbal = cfg
	return nil
}

// 从环境变量加载配置
func LoadConfigFromEnv() error {
	cfg := newDefaultConfig()
	if err := env.Parse(cfg); err != nil {
		return err
	}
	golbal = cfg
	return nil
}
