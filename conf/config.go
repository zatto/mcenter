package conf

import (
	"context"
	"fmt"
	"sync"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Config struct {
	App   *app     `toml:"app"`
	Mongo *mongodb `toml:"mongodb"`
}

func newDefaultConfig() *Config {
	return &Config{
		App:   newDefaultApp(),
		Mongo: newDefaultMongodb(),
	}
}

type app struct {
	Name string `toml:"name" env:"APP_NAME"`
	HTTP *http  `toml:"http"`
	GRPC *grpc  `toml:"grpc"`
}

func newDefaultApp() *app {
	return &app{
		Name: "mcenter",
		HTTP: newDefaultHttp(),
		GRPC: newDefaultGrpc(),
	}
}

type http struct {
	Host string `toml:"host" env:"HTTP_HOST"`
	Port string `toml:"port" env:"HTTP_PORT"`
}

func (h *http) Addr() string {
	return fmt.Sprintf("%s:%s", h.Host, h.Port)
}

func newDefaultHttp() *http {
	return &http{
		Host: "127.0.0.1",
		Port: "8010",
	}
}

type grpc struct {
	Host string `toml:"host" env:"GRPC_HOST"`
	Port string `toml:"port" env:"GRPC_PORT"`
}

func (rpc *grpc) Addr() string {
	return fmt.Sprintf("%s:%s", rpc.Host, rpc.Port)
}

func newDefaultGrpc() *grpc {
	return &grpc{
		Host: "127.0.0.1",
		Port: "18010",
	}
}

type mongodb struct {
	Endpoints  []string `toml:"endpoints" env:"MONGO_ENDPOINTS" envSeparator:","`
	UserName   string   `toml:"username" env:"MONGO_USERNAME"`
	Password   string   `toml:"password" env:"MONGO_PASSWORD"`
	Database   string   `toml:"database" env:"MONGO_DATABASE"`
	AuthSource string   `toml:"auth_source" env:"MONGO_AUTH_SOURCE"`

	lock   sync.Mutex
	client *mongo.Client
}

func newDefaultMongodb() *mongodb {
	return &mongodb{
		Database:   "mcenter",
		AuthSource: "mcenter",
		Endpoints:  []string{"192.168.200.10:27017"},
	}
}

func (m *mongodb) GetDB() (*mongo.Database, error) {
	conn, err := m.Client()
	if err != nil {
		return nil, err
	}
	return conn.Database(m.Database), nil
}

func (m *mongodb) Client() (*mongo.Client, error) {
	m.lock.Lock()
	defer m.lock.Unlock()

	if m.client == nil {
		conn, err := m.getClient()
		if err != nil {
			return nil, err
		}
		m.client = conn
	}

	return m.client, nil
}

// AuthSource: 用于认证的数据, 使用的Database
func (m *mongodb) getClient() (*mongo.Client, error) {
	opts := options.Client()

	cred := options.Credential{
		AuthSource: m.AuthSource,
	}

	if m.UserName != "" && m.Password != "" {
		cred.Username = m.UserName
		cred.Password = m.Password
		cred.PasswordSet = true
		opts.SetAuth(cred)
	}
	opts.SetHosts(m.Endpoints)
	opts.SetConnectTimeout(5 * time.Second)

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*5))
	defer cancel()

	// Connect to MongoDB
	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		return nil, fmt.Errorf("new mongodb client error, %s", err)
	}

	// Ping下MongoDB
	if err = client.Ping(ctx, nil); err != nil {
		return nil, fmt.Errorf("ping mongodb server(%s) error, %s", m.Endpoints, err)
	}

	return client, nil
}
