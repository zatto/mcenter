package auth

import (
	"context"

	"gitee.com/zatto/mcenter/apps/service"
	"github.com/infraboard/mcube/app"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

type grpcAuther struct {
	service service.ServiceManager
}

func NewGrpcAuther() *grpcAuther {
	return &grpcAuther{
		service: app.GetInternalApp(service.AppName).(service.ServiceManager),
	}
}

func (a *grpcAuther) GetClientCredentailsFromMeta(md metadata.MD) (clientId, clientSecret string) {
	cids := md.Get(service.ClientHeaderKey)
	sids := md.Get(service.ClientSecretKey)
	if len(cids) > 0 {
		clientId = cids[0]
	}
	if len(sids) > 0 {
		clientSecret = sids[0]
	}
	return
}

// grpc auth中间件
func (a *grpcAuther) AuthFunc(
	ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler,
) (
	resp interface{}, err error) {

	// 从metadata中获取请求参数
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, status.Error(codes.PermissionDenied, "rpc未认证")
	}

	clientId, clientSecret := a.GetClientCredentailsFromMeta(md)
	if clientId == "" || clientSecret == "" {
		return nil, status.Error(codes.PermissionDenied, "rpc未认证")
	}

	// 从服务端获取凭证
	svc, err := a.service.DescribeService(ctx, &service.DescribeServiceRequest{
		DescribeBy:    service.DESCRIBE_BY_SERVICE_CREDENTAIL_ID,
		DescribeValue: clientId,
	})
	if err != nil {
		// return nil, status.Errorf(codes.PermissionDenied, "认证异常: %s", err)
		return nil, status.Errorf(codes.PermissionDenied, "rpc认证失败: clientId不存在")
	}

	// 校验凭证
	if clientSecret != svc.Credentail.ClientSecret {
		return nil, status.Error(codes.PermissionDenied, "rpc认证失败: clientSecret错误")
	}

	// 继续下个中间件
	resp, err = handler(ctx, req)

	return resp, err
}
