package protocol

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitee.com/zatto/mcenter/apps/endpoint"
	"gitee.com/zatto/mcenter/common/logger"
	"gitee.com/zatto/mcenter/conf"
	"gitee.com/zatto/mcenter/protocol/auth"
	"gitee.com/zatto/mcenter/swagger"
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/app"
)

type HTTPService struct {
	r      *restful.Container
	c      *conf.Config
	server *http.Server
}

func NewHTTPService() *HTTPService {
	r := restful.DefaultContainer

	// 加载跨域中间件
	cors := restful.CrossOriginResourceSharing{
		AllowedHeaders: []string{"*"},
		AllowedDomains: []string{"*"},
		AllowedMethods: []string{"HEAD", "OPTIONS", "GET", "POST", "PUT", "PATCH", "DELETE"},
		CookiesAllowed: false,
		Container:      r,
	}
	r.Filter(cors.Filter)

	// 加载认证中间件
	r.Filter(auth.NewHttpAuthor().AuthFunc)

	server := &http.Server{
		ReadHeaderTimeout: 60 * time.Second,
		ReadTimeout:       60 * time.Second,
		WriteTimeout:      60 * time.Second,
		IdleTimeout:       60 * time.Second,
		MaxHeaderBytes:    1 << 20, // 1M
		Addr:              conf.C().App.HTTP.Addr(),
		Handler:           r,
	}

	return &HTTPService{
		r:      r,
		server: server,
		c:      conf.C(),
	}
}

// mcenter/api/users/v1
// mcenter/api/tokens/v1
func (s *HTTPService) PathPrefix() string {
	return fmt.Sprintf("/%s/api", s.c.App.Name)
}

func (s *HTTPService) Start() error {
	// 装置所有子服务路由
	app.LoadRESTfulApp(s.PathPrefix(), s.r)

	// 注册所有endpoint
	es := endpoint.NewRegistryRequest()
	wss := s.r.RegisteredWebServices()
	for i := range wss {
		ws := wss[i]
		routes := ws.Routes()
		for m := range routes {
			route := routes[m]
			ep := &endpoint.CreateEndpointRequest{
				ServiceId: "cfsrgnh3n7pi7u2is87g",
				Method:    route.Method,
				Path:      route.Path,
				Operation: route.Operation,
			}
			es.Add(ep)
		}
	}

	fmt.Println(es)

	// 装置api文档
	// API Doc
	config := restfulspec.Config{
		WebServices:                   restful.RegisteredWebServices(), // you control what services are visible
		APIPath:                       "/apidocs.json",
		PostBuildSwaggerObjectHandler: swagger.Docs,
		DefinitionNameHandler: func(name string) string {
			if name == "state" || name == "sizeCache" || name == "unknownFields" {
				return ""
			}
			return name
		},
	}
	s.r.Add(restfulspec.NewOpenAPIService(config))
	logger.L().Info().Msgf("Get the API using http://%s%s", s.c.App.HTTP.Addr(), config.APIPath)

	// 启动 HTTP服务
	logger.L().Info().Msgf("HTTP服务启动成功, 监听地址: %s", s.server.Addr)
	if err := s.server.ListenAndServe(); err != nil {
		if err == http.ErrServerClosed {
			logger.L().Info().Msgf("service is stopped")
		}
		return fmt.Errorf("start service error, %s", err.Error())
	}
	return nil
}

func (s *HTTPService) Stop() error {
	logger.L().Info().Msgf("start graceful shutdown")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	// 优雅关闭HTTP服务
	if err := s.server.Shutdown(ctx); err != nil {
		logger.L().Error().Msgf("graceful shutdown timeout, force exit")
	}
	return nil
}
